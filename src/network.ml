(* Implémentation avec un serveur et plusieurs clients *)

type response = Ack | Value of string | Channel of int | NotFound
type request = Put of int * string | Get of int | NewChan

let sock_addr = ref (Unix.ADDR_UNIX "")

(* Le mode de parallélisme *)
type p_mode = Thread | Process | Sequence
let p_mode = ref Thread

let set_mode m = p_mode := m

module Server = struct
  (* Les requêtes du côté serveur, l'adresse du client et la requête *)
  type client = Client of Unix.file_descr * Unix.sockaddr * request

  (* Envoie la réponse du serveur *)
  let send_response res socket =
    let channel = Unix.out_channel_of_descr socket in
    (match res with
    | Ack       -> output_string channel "ACK\n"
    | Value s   ->
        let len = String.length s in
        output_string channel "VALUE\n";
        output_string channel (string_of_int len ^ "\n");
        output_string channel (s ^ "\n")
    | Channel c ->
        output_string channel "CHANNEL\n";
        output_string channel (string_of_int c ^ "\n")
    | NotFound  -> output_string channel "NOT FOUND\n");
    flush channel;
    Unix.close socket

  (* Le stockage des valeurs de côté serveur *)
  let nb_clients = ref 0
  let queues : (int, string Queue.t) Hashtbl.t = Hashtbl.create 4

  let service_request = function
    | NewChan       ->
        incr nb_clients;
        Hashtbl.add queues !nb_clients (Queue.create ());
        Channel !nb_clients
    | Get q_id      ->
        if q_id > !nb_clients then
          NotFound
        else
          let q = Hashtbl.find queues q_id in
          if Queue.is_empty q then
            NotFound
          else
            let v = Queue.pop q in
            Value v
    | Put (q_id, v) ->
        let q = Hashtbl.find queues q_id in
        Queue.push v q;
        Ack

  (* Le décodage *)
  let decode_request channel =
    let s = input_line channel in
    (* print_endline s; *)
    match s with
    | "PUT"         ->
        let q_id = int_of_string (input_line channel) in
        let len = int_of_string (input_line channel) in
        let v = really_input_string channel len in
        assert (q_id <= !nb_clients);
        Put (q_id, v)
    | "GET"         ->
        let q_id = int_of_string (input_line channel) in
        Get q_id
    | "NEW CHANNEL" -> NewChan
    | s             ->
        print_endline s;
        assert false

  (* Initialise le serveur à partir de l'adresse donnée *)
  let init_server sock_addr =
    let sock_domain = Unix.domain_of_sockaddr sock_addr in
    let socket = Unix.socket sock_domain Unix.SOCK_STREAM 0 in
    Unix.setsockopt socket Unix.SO_REUSEADDR true;
    Unix.bind socket sock_addr;
    Unix.listen socket 5;
    while true do
      if Unix.select [ socket ] [] [] 0.1 <> ([], [], []) then
        let sock_client, _ = Unix.accept socket in
        let channel = Unix.in_channel_of_descr sock_client in
        let req = decode_request channel in
        send_response (service_request req) sock_client
    done
end

module Client = struct
  (* Décodage d'une réponse du serveur *)
  let rec decode_response sock =
    if Unix.select [ sock ] [] [] (-0.1) <> ([], [], []) then (
      let channel = Unix.in_channel_of_descr sock in
      let s = input_line channel in
      (* print_endline s; *)
      let res =
        match s with
        | "ACK"       -> Ack
        | "VALUE"     ->
            let len = int_of_string (input_line channel) in
            let v = really_input_string channel len in
            Value v
        | "NOT FOUND" -> NotFound
        | "CHANNEL"   ->
            let q_id = int_of_string (input_line channel) in
            Channel q_id
        | _           -> assert false
      in
      Unix.close sock;
      res
    ) else (
      if !p_mode <> Sequence then Thread.yield ();
      decode_response sock
    )

  let connect addr =
    flush_all ();
    let sock_domain = Unix.domain_of_sockaddr addr in
    let socket = Unix.socket sock_domain Unix.SOCK_STREAM 0 in
    let _ = Unix.connect socket addr in
    socket

  let send_request req addr =
    let socket = connect addr in
    let channel = Unix.out_channel_of_descr socket in
    (match req with
    | NewChan       -> output_string channel "NEW CHANNEL\n"
    | Get q_id      ->
        output_string channel "GET\n";
        output_string channel (string_of_int q_id ^ "\n")
    | Put (q_id, v) ->
        output_string channel "PUT\n";
        output_string channel (string_of_int q_id ^ "\n");
        output_string channel (string_of_int (String.length v) ^ "\n");
        output_string channel (v ^ "\n"));
    flush channel;
    socket

  let init_client sockaddr task =
    sock_addr := sockaddr;
    task ()
end

module Network : Khan.S = struct
  type 'a value = Concrete of 'a | Wait of 'a process

  and 'a process = unit -> 'a value

  (* Le serveur maintient une table des files et le port est le numéro *)
  type 'a in_port = int
  type 'a out_port = int

  let new_channel () =
    let socket = Client.send_request NewChan !sock_addr in
    match Client.decode_response socket with
    | Channel q_id -> (q_id, q_id)
    | _            -> assert false

  let put x p () =
    let socket =
      Client.send_request (Put (p, Marshal.to_string x [])) !sock_addr
    in
    match Client.decode_response socket with
    | Ack -> Concrete ()
    | _   -> assert false

  let rec get p () =
    let socket = Client.send_request (Get p) !sock_addr in
    match Client.decode_response socket with
    | NotFound ->
        if !p_mode <> Sequence then Thread.yield ();
        Wait (get p)
    | Value s  -> Concrete (Marshal.from_string s 0)
    | _        -> assert false

  let rec run p =
    match p () with
    | Concrete a -> a
    | Wait p'    ->
        if !p_mode <> Sequence then Thread.yield ();
        run p'

  let rec doco p_list () =
    match !p_mode with
    | Sequence -> (
        (* Exécution séquentielle *)

        (* Évalue le processus et renvoie les processus non terminés *)
        let eval p_list p =
          match p () with
          | Concrete () -> p_list
          | Wait p'     -> p' :: p_list
        in
        (* On itère l'évaluation sur la liste des processus *)
        match p_list with
        | [] -> Concrete ()
        | _  -> Wait (doco (List.fold_left eval [] p_list)))
    | Process  ->
        (* Exécution dans des processus lourds *)
        let rec aux p_list n =
          match p_list with
          | []           ->
              Concrete
                (for _ = 1 to n do
                   let _ = Unix.wait () in
                   ()
                 done)
          | p :: p_list' -> (
              match Unix.fork () with
              | 0 ->
                  Client.init_client !sock_addr (fun () -> run p);
                  exit 0
              | _ -> aux p_list' (n + 1))
        in
        aux p_list 0
    | Thread   ->
        (* Exécution dans des processus légers *)
        let ths =
          List.map
            (fun (p : unit process) ->
              Thread.create
                (fun () -> Client.init_client !sock_addr (fun () -> run p))
                ())
            p_list
        in
        Concrete (List.iter (fun th -> Thread.join th) ths)

  let return a () = Concrete a

  let rec bind p f () =
    match p () with
    (* On effectue une seule action si cela est possible *)
    | Concrete a -> Wait (f a)
    | Wait p' ->
        if !p_mode <> Sequence then Thread.yield ();
        Wait (bind p' f)
end
