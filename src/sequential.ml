(* Implémentation séquentielle *)

module Sequential : Khan.S = struct
  (* Une valeur est soit concrète, soit la promesse d'une valeur concrète *)
  type 'a value = Value of 'a | Wait of 'a process

  and 'a process = unit -> 'a value

  type 'a channel = 'a Queue.t
  type 'a in_port = 'a channel
  type 'a out_port = 'a channel

  let new_channel () =
    let q = Queue.create () in
    (q, q)

  let put x p () = Value (Queue.push x p)

  let rec get p () =
    try Value (Queue.take p) with
    | Queue.Empty -> Wait (get p)

  let rec doco p_list () =
    (* Évalue le processus et renvoie les processus non terminés *)
    let eval p_list p =
      match p () with
      | Value _ -> p_list
      | Wait p' -> p' :: p_list
    in
    (* On itère l'évaluation sur la liste des processus *)
    match p_list with
    | [] -> Value ()
    | _  -> Wait (doco (List.fold_left eval [] p_list))

  let return a () = Value a

  let rec bind p f () =
    match p () with
    (* On effectue une action si cela est possible *)
    | Value a -> Wait (f a)
    | Wait p' -> Wait (bind p' f)

  let rec run p =
    match p () with
    | Value a -> a
    | Wait p' -> run p'
end
