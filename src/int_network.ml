open Network
open Integers

module NetworkInt = Integers (Network)

let main =
  let is_server = ref false in
  let server_address = ref "127.0.0.1" in
  let port = ref 12345 in
  let mode = ref "thread" in
  let speclist =
    [
      ("-s", Arg.Set is_server, "lancer le serveur");
      ("-a", Arg.Set_string server_address, "addresse IP du serveur");
      ("-p", Arg.Set_int port, "port utilisé");
      ( "-m",
        Arg.Set_string mode,
        "Mode de parallélisme, choix possibles : thread, process, sequence" );
    ]
  in
  let usage = "Usage : " in
  Arg.parse speclist (fun _ -> failwith "No anonymous arguments allowed") usage;
  if !is_server then (
    print_endline "Lancement du serveur...";
    let ip_addr = Unix.inet_addr_any in
    let addr = Unix.ADDR_INET (ip_addr, !port) in
    print_endline (Unix.string_of_inet_addr ip_addr);
    Server.init_server addr
  ) else (
    (match !mode with
    | "thread"   -> ()
    | "process"  -> set_mode Process
    | "sequence" -> set_mode Sequence
    | _          ->
        print_endline "Mode inconnu";
        exit 1);
    print_endline "Lancement du client...";
    let ip_addr = Unix.inet_addr_of_string !server_address in
    let addr = Unix.ADDR_INET (ip_addr, !port) in
    Client.init_client addr (fun () -> Network.run NetworkInt.main)
  )
