(* Tests pour l'implémentation séquentielle *)

open Sequential
open Integers

module SequentialIntegers = Integers (Sequential)

let _ = Sequential.run SequentialIntegers.main
