(* Tests pour l'implémentation avec tuyaux *)

open Pipes
open Integers

module PipesIntegers = Integers (Pipes)

let _ = Pipes.run PipesIntegers.main
