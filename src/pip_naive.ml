(* Tests pour l'implémentation naïve *)

open Naive
open Pip

module NaivePip = Pip (Naive)

let _ = Naive.run (NaivePip.main Sys.argv.(1) Sys.argv.(2))
