(* Exemple du sujet *)

module Integers (K : Khan.S) = struct
  module Lib = Khan.Lib (K)
  open Lib

  let integers (qo : int K.out_port) : unit K.process =
    let rec loop n = K.put n qo >>= fun () -> loop (n + 1) in
    loop 2

  let output n (qi : int K.in_port) : unit K.process =
    let rec loop () =
      K.get qi >>= fun v ->
      Format.printf "%d - %d@." n v;
      flush_all ();
      loop ()
    in
    loop ()

  let main : unit K.process =
    K.doco
      [
        ( delay K.new_channel () >>= fun (qi, qo) ->
          K.doco [ integers qo; output 1 qi ] );
        ( delay K.new_channel () >>= fun (qi, qo) ->
          K.doco [ integers qo; output 2 qi ] );
      ]
end
