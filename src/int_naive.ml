(* Tests pour l'implémentation naïve *)

open Naive
open Integers

module NaiveIntegers = Integers (Naive)

let _ = Naive.run NaiveIntegers.main
