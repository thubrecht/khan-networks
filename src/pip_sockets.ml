(* Tests pour l'implémentation avec sockets (en utilisant des fichiers) *)

open Socket
open Pip

module SocketPip = Pip (Sockets)

let _ = Sockets.run (SocketPip.main Sys.argv.(1) Sys.argv.(2))
