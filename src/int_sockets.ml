(* Tests pour l'implémentation avec sockets (en utilisant des fichiers) *)

open Socket
open Integers

module SocketIntegers = Integers (Sockets)

let _ = Sockets.run SocketIntegers.main
