(* Tests pour l'implémentation avec sockets (en utilisant des fichiers) *)

open Pipes
open Pip

module PipesPip = Pip (Pipes)

let _ = Pipes.run (PipesPip.main Sys.argv.(1) Sys.argv.(2))
