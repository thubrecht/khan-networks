(* Version parallèle de Picture-in-Picture *)

type pixel = int * int * int
type line = pixel list
type image = { width : int; height : int; pixels : line list }
type c_pixel = pixel * (int * int) (* Un pixel avec ses coordonnées *)

(* Lit une image au format PPM *)
let input_int chan =
  let rec input_int c acc =
    if '0' <= c && c <= '9' then
      input_int (input_char chan)
        ((10 * acc) + (int_of_char c - int_of_char '0'))
    else
      acc
  in
  let c = input_char chan in
  if not ('0' <= c && c <= '9') then
    failwith "input_int"
  else
    let n = input_int c 0 in
    let _ = seek_in chan (pos_in chan - 1) in
    n

let rec whitespace chan =
  match input_char chan with
  | ' '
  | '\t'
  | '\r'
  | '\n' ->
      whitespace chan
  | _ -> seek_in chan (pos_in chan - 1)

let input_pixel chan =
  let r = int_of_char (input_char chan) in
  let g = int_of_char (input_char chan) in
  let b = int_of_char (input_char chan) in
  (r, g, b)

let input_pixel_line chan width =
  let rec input_pixel_line n acc =
    if n > 0 then
      input_pixel_line (n - 1) (input_pixel chan :: acc)
    else
      List.rev acc
  in
  input_pixel_line width []

let input_pixel_matrix chan height width =
  let rec input_pixel_matrix n acc =
    if n > 0 then
      input_pixel_matrix (n - 1) (input_pixel_line chan width :: acc)
    else
      List.rev acc
  in
  input_pixel_matrix height []

let input_ppm chan =
  if input_char chan <> 'P' then failwith "input: bad format";
  if input_char chan <> '6' then failwith "input: bad format";
  whitespace chan;
  let width = input_int chan in
  whitespace chan;
  let height = input_int chan in
  whitespace chan;
  let maxval = input_int chan in
  if maxval >= 256 then failwith "input: too much colors";
  whitespace chan;
  let pixels = input_pixel_matrix chan height width in
  { width; height; pixels }

let read_ppm file =
  let chan = open_in_bin file in
  let img = input_ppm chan in
  close_in chan;
  img

let div x = (x / 2) + (x mod 2)

(* Module pour les opérations parallèles *)
module Pip (K : Khan.S) = struct
  module Lib = Khan.Lib (K)
  open Lib

  type instruction = Draw of c_pixel | Done

  (* Affichage d'une image de façon parallèle *)
  let draw (qi : instruction K.in_port) : unit K.process =
    let rec loop () =
      K.get qi >>= function
      | Done                     ->
          Graphics.synchronize ();
          K.return ()
      | Draw ((r, g, b), (i, j)) ->
          Graphics.set_color (Graphics.rgb r g b);
          Graphics.plot (i - 1) (j - 1);
          loop ()
    in
    loop ()

  (* Transmission d'une image pixel par pixel sur un canal *)
  let send_img img qo : unit K.process =
    let rec loop = function
      | [], []     -> K.return ()
      | [], t :: q -> loop (t, q)
      | t :: q, l  -> K.put t qo >>= fun () -> loop (q, l)
    in
    loop ([], img.pixels)

  let receive_image w h (qi : pixel K.in_port) (qo : instruction K.out_port) =
    let rec transmit i j =
      match (i, j) with
      | 0, 1 -> K.put Done qo
      | 0, _ -> transmit w (j - 1)
      | _    ->
          K.get qi >>= fun p ->
          K.put (Draw (p, (i, j))) qo >>= fun () -> transmit (i - 1) j
    in
    transmit w h

  let download_img w h qi =
    let rec receive line pixels = function
      | _, 0 -> K.return { width = w; height = h; pixels }
      | 0, j -> receive [] (line :: pixels) (w, j - 1)
      | i, j -> K.get qi >>= fun p -> receive (p :: line) pixels (i - 1, j)
    in
    receive [] [] (w, h)

  (* On divise la taille d'une image par deux *)
  let rec half_sampler l =
    match l with
    | []           -> []
    | [ x ]        -> [ x ]
    | x :: _ :: l' -> x :: half_sampler l'

  let vf img =
    K.return
      {
        height = div img.height;
        width = img.width;
        pixels = half_sampler img.pixels;
      }

  let hf img =
    let rec map m =
      match m with
      | []     -> []
      | x :: l -> half_sampler x :: map l
    in
    K.return
      { height = img.height; width = div img.width; pixels = map img.pixels }

  let downscale img = hf img >>= vf

  (* On dédouble un flux *)
  let split qi qo_a qo_b : unit K.process =
    let rec loop () =
      K.get qi >>= fun x -> K.doco [ K.put x qo_a; K.put x qo_b ] >>= loop
    in
    loop ()

  (* On filtre le flux de pixels selon le flux du masque *)
  let sample (qp : pixel K.in_port) (qm : bool K.in_port)
      (qo : pixel K.out_port) : unit K.process =
    let rec loop () =
      K.get qm >>= function
      | true  -> (K.get qp >>= fun p -> K.put p qo) >>= fun () -> loop ()
      | false -> K.get qp >>= fun _ -> loop ()
    in
    loop ()

  (* On fusionne les deux flux en fonction du masque *)
  let merge (qs : pixel K.in_port) (qb : pixel K.in_port) (qm : bool K.in_port)
      (qo : pixel K.out_port) : unit K.process =
    let rec loop () =
      K.get qm >>= function
      | true  -> K.get qb >>= (fun p -> K.put p qo) >>= loop
      | false -> K.get qs >>= (fun p -> K.put p qo) >>= loop
    in
    loop ()

  (* On envoie le masque *)
  let send_mask (x_1, y_1) (x_2, y_2) qo =
    let rec mask_bit i j =
      match (i, j) with
      | _ when i == x_1 && j == y_1 -> K.return ()
      | _ when i == x_1 -> mask_bit 0 (j + 1)
      | _ ->
          (* On affiche l'image dans le coin en bas à gauche, sachant que (0, 0)
           * est le coin en haut à gauche *)
          K.put (x_1 - i > x_2 || y_1 - j > y_2) qo >>= fun () ->
          mask_bit (i + 1) j
    in
    mask_bit 0 0

  let pip img_a img_b qo =
    (* On incruste l'image B dans l'image A en réduisant la taille de l'image B *)
    let t_a = (img_a.width, img_a.height) in
    let t_b = (div img_b.width, div img_b.height) in
    (* Image A *)
    delay K.new_channel () >>= fun (qi_a, qo_a) ->
    delay K.new_channel () >>= fun (qi_as, qo_as) ->
    (* Image B *)
    delay K.new_channel () >>= fun (qi_b, qo_b) ->
    (* Mask *)
    delay K.new_channel () >>= fun (qi_m, qo_m) ->
    delay K.new_channel () >>= fun (qi_m1, qo_m1) ->
    delay K.new_channel () >>= fun (qi_m2, qo_m2) ->
    K.doco
      [
        (downscale img_b >>= fun img -> send_img img qo_b);
        send_mask t_a t_b qo_m;
        split qi_m qo_m1 qo_m2;
        send_img img_a qo_a;
        sample qi_a qi_m1 qo_as;
        merge qi_b qi_as qi_m2 qo;
      ]

  let main i_b i_s : unit K.process =
    let img = read_ppm i_s in
    let bigimg = read_ppm i_b in
    Graphics.open_graph "";
    Graphics.auto_synchronize true;
    Graphics.clear_graph ();
    Graphics.resize_window bigimg.width bigimg.height;
    let ((w_b, h_b) (* as t_b *)) = (bigimg.width, bigimg.height) in
    (* let t_s = (div img.width, div img.height) in *)
    (* (1* Small picture *1) *)
    (* delay K.new_channel () >>= fun (qi_s, qo_s) -> *)
    (* (1* Big picture *1) *)
    (* delay K.new_channel () >>= fun (qi_b, qo_b) -> *)
    (* delay K.new_channel () >>= fun (qi_b1, qo_b1) -> *)
    (* (1* Mask *1) *)
    (* delay K.new_channel () >>= fun (qi_m, qo_m) -> *)
    (* delay K.new_channel () >>= fun (qi_m1, qo_m1) -> *)
    (* delay K.new_channel () >>= fun (qi_m2, qo_m2) -> *)
    (* Résultat *)
    delay K.new_channel () >>= fun (qi_r, qo_r) ->
    (* Affichage *)
    delay K.new_channel () >>= fun (qi_d, qo_d) ->
    K.doco
      [
        (* (downscale img >>= fun img -> send_img img qo_s); *)
        (* send_mask t_b t_s qo_m; *)
        (* split qi_m qo_m1 qo_m2; *)
        (* send_img bigimg qo_b; *)
        (* sample qi_b qi_m1 qo_b1; *)
        (* merge qi_s qi_b1 qi_m2 qo_r; *)
        pip bigimg img qo_r;
        receive_image w_b h_b qi_r qo_d;
        draw qi_d;
      ]
end
