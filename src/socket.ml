(* Implémentation avec sockets Unix *)

module Sockets : Khan.S = struct
  type 'a process = unit -> 'a

  type 'a in_port = in_channel
  type 'a out_port = out_channel

  let curr_socket = ref 0

  (* On rajoute un préfixe car sinon on peut avoir plusieurs sockets avec le même
   * noms à cause de l'appel à fork *)
  let prefix = ref "-"

  let get_socket () =
    incr curr_socket;
    Unix.ADDR_UNIX (!prefix ^ string_of_int !curr_socket ^ ".sock")

  let new_channel () =
    let sock_addr = get_socket () in
    let sock_in = Unix.socket Unix.PF_UNIX Unix.SOCK_STREAM 0 in
    let socket = Unix.socket Unix.PF_UNIX Unix.SOCK_STREAM 0 in
    Unix.bind socket sock_addr;
    Unix.listen socket 1;
    Unix.connect sock_in sock_addr;
    let sock_out, _ = Unix.accept socket in
    (Unix.in_channel_of_descr sock_in, Unix.out_channel_of_descr sock_out)

  let put a qo () = output_value qo a

  let get qi () = input_value qi

  let doco p_list () =
    let rec aux p_list n =
      match p_list with
      | []           ->
          for _ = 1 to n do
            let _ = Unix.wait () in
            ()
          done
      | p :: p_list' -> (
          match Unix.fork () with
          | 0 ->
              prefix := "1" ^ !prefix;
              p ();
              exit 0
          | _ ->
              prefix := "0" ^ !prefix;
              aux p_list' (n + 1))
    in
    aux p_list 0

  let return v () = v

  let bind p f () = f (p ()) ()

  let run p = p ()
end
