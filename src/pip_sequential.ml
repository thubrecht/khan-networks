(* Tests pour l'implémentation séquentielle *)

open Sequential
open Pip

module SequentialPip = Pip (Sequential)

let _ = Sequential.run (SequentialPip.main Sys.argv.(1) Sys.argv.(2))
