(* Implémentation avec tuyaux Unix *)

module Pipes : Khan.S = struct
  type 'a process = unit -> 'a
  type 'a in_port = in_channel
  type 'a out_port = out_channel

  let new_channel () =
    let qi, qo = Unix.pipe () in
    (Unix.in_channel_of_descr qi, Unix.out_channel_of_descr qo)

  let put a qo () = output_value qo a

  let get qi () = input_value qi

  let doco p_list () =
    let rec aux p_list n =
      match p_list with
      | []           ->
          for _ = 1 to n do
            let _ = Unix.wait () in
            ()
          done
      | p :: p_list' -> (
          match Unix.fork () with
          | 0 ->
              p ();
              exit 0
          | _ -> aux p_list' (n + 1))
    in
    aux p_list 0

  let return v () = v

  let bind p f () = f (p ()) ()

  let run p = p ()
end
