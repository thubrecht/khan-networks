# Makefile pour le projet Khan

all:
	$(MAKE) -C src/
	@echo "Copie des exécutables..."
	@cp -f src/_build/default/*.exe ./

clean:
	$(MAKE) -C src/ clean
	@echo "Suppression des exécutables..."
	@rm -f ./*.exe
	@echo "Suppression des prises..."
	@rm -f ./*.sock
